# Using the Compiler

zrpc is written in Rust, and uses the Cargo build system. To learn more about Cargo, see [https://doc.rust-lang.org/cargo/](https://doc.rust-lang.org/cargo/).

The compiler can be downloaded at [https://codeberg.org/zbwells/zrpc](https://codeberg.org/zbwells/zrpc).


If you don't wish to install the compiler anywhere, do something like this:

```
cargo run <program name>.rpc
```

with the program maybe being something from the sample programs directory (/prog in the Git repo). The compiler will then write the executable to "zrpc_out", or if you want, you can specify another name for the output by providing two arguments on the command-line:

```
cargo run <program name> <output name>
```

There is currently no robust command line arguments system in place, nor any interesting options.

The command-line usage is likely subject to change, as it's the place where the least work has been done.


