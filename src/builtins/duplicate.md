# d

* arity: 0
* incptr: 1

Duplicates whichever object is currently on top of stack.

Usage:

```
<object to duplicate> d
```

Example:

```
5 4 d
```

would resolve to

```
5 4 4
```
