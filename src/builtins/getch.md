# getch

* arity: 0
* incptr: 1

Gets a character from stdin. (command-line input)

Usage:

```
getch printchr recurse
```

will infinitely get characters from the CLI and print them out.
