# q

* arity: n/a
* incptr: n/a

Exits the program, equivalent to `exit(int)` in C.

Usage:

```
q
```

