# printstr

* arity: 1
* incptr: 1

Prints out a string.

Usage:

```
"this string will be printed" printstr
```

