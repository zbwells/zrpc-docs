# strword

Fetches a word from a string. A word is any ascii text separated by spaces.
In the string "I eat food", strword would first return "I", then "eat", and then "food", each on subsequent calls.

Usage:

```
"I eat food" strword pop strword printstr
```

Will print `eat`.

TODO: Make this page better.
