# atof

* arity: 1
* incptr: 1

Takes a string and converts its characters to a single decimal number, if possible.

Usage:

```
"12.5" atof
```

is equivalent to `12.5`.
