# ref

* arity: 1
* incptr: 1

Accepts two arguments, despite having an arity of 1. Fetches whatever character is at the index provided in a string. The first argument is the string, the second argument is the position to look for. The position argument is popped off the stack, but the string is not.

Usage:

```
"garbage" 2 ref printchr
```

will print `r`.
