# print

* arity: 1
* incptr: 1

Prints a number, but doesn't pop the number off the stack.

Usage:

```
5 print
```
