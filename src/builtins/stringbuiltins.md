# Strings and Text Processing

This section contains reference for String-related built-in functions.

- [append](append.md)
- [ascii](ascii.md)
- [atoc]( atoc.md)
- [atof]( atof.md)
- [cheq]( cheq.md)
- [emptybuf]( emptybuf.md)
- [fprintstr]( fprintstr.md)
- [getch]( getch.md)
- [getstr]( getstr.md)
- [print]( print.md)
- [printchr]( printchr.md)
- [printstr]( printstr.md)
- [ref]( ref.md)
- [streq]( printchr.md)
- [strlen]( strlen.md)
- [strword]( strword.md)
