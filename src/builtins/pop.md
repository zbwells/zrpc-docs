# pop

* arity: 1
* incptr: 0

Decrements the stack pointer by one, effectively popping an object off the stack.

Example:

```
"words" printstr pop
```

In the above case, `printstr` would print `"words"`, and then `pop` would remove the string from the stack.


