# head

* arity: 0
* incptr: 1

Will get the first character from a string.

Example:

```
"garbage" head printchr
```

will print `g`.
