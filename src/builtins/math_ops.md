# Math Operators

In zrpc, mathematical operators are functions, and so they have the same properties as normal built-in functions do.

## +

* arity: 2
* incptr: 1

Addition.

```
5 4 +
```

will resolve to `9`.

## -

Subtraction.

```
5 4 -
```

will resolve to `1`.

## *

Multiplication.

```
5 4 *
```

will resolve to `20`.

## /

Division.

```
20 5 /
```

will resolve to `4`.

## ^

Exponentiation.

```
5 2 ^
```

will resolve to `25`.

## %

Modulus.

```
5 4 %
```

will resolve to `1`.

## =

Print number and pop number off of stack.
Useful for getting those pesky numbers out of your way after calculations are done.

```
5 2 ^ 10 * 50 + =
```

Will leave no remaining items on the stack, but will print `300`.

