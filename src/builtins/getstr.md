# getstr

* arity: 0
* incptr: 1

Dynamically allocates space for a string, and then fetches a newline-terminated string from stdin.

Usage:

```
getstr printstr pop
```
