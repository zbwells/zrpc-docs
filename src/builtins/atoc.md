# atoc

* arity: 1
* incptr: 1

Converts a character to its ascii integer value.

Usage:

```
"ABC" 0 ref atoc =
```

will print out `65`.
