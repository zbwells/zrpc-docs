# wfopen

* arity: 1
* incptr: 1

Opens a file in write mode.

Usage:

```
"file.txt" wfopen "hello" fprintstr
```

will put `hello` in file.txt.
