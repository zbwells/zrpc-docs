# Logical Operators

* arity: 2
* incptr: 0

In zrpc, logical operators are functions, and so they have the same properties as normal built-in functions. Each logical operator has an arity of 2 and an incptr of 0.

## ?{

Equivalent to "if (num1 == num2) {" in C, where both objects popped off the stack are numeric.

Usage:

```
1 1 ?{
    "equal numbers" printstr q
}
```

All logical operators work the same way, and there are no "and" or "or" operators.

## !?{

Equivalent to "if (num1 != num2) {" in C.

Usage:

```
1 2 !?{
    "unequal numbers" printstr q
}
```

## <?{

Usage:

```
1 2 <?{
    "1 is less than 2" printstr q
}
```

## >?{

Usage:

```
2 1 >?{
    "2 is greater than 1" printstr q
}
```

