# getarg

* arity: 0
* incptr: 1

Fetches an argument from the command-line. Fetches the next argument sequentially with each usage.

Example:

```
getarg printstr streq "\0" 1 ?{ recurse }
```

will print out every argument on the CLI until it cannot find any more.
