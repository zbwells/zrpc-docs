# printchr

* arity: 1
* incptr: 1

Prints out a character.

```
'a' printchr
```
