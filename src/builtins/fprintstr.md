# fprintstr

* arity: 1
* incptr: 1

Print a string to a specific file. Takes two arguments, the first being the file, and the second being the string to print to the file. The arity is still 1, because the file does not get removed from the stack. Behavior subject to change.

Usage:

```
"file.txt" wfopen "some words\n" fprintstr
```
