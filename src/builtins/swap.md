# swap

* arity: 0
* incptr: 0

Swaps the two most recent positions on the stack with each other.

Example:

```
4 5 swap
```

will result in:

```
5 4
```
