# recurse

* arity: n/a
* incptr: n/a

Calls the top-level program present in the source, allowing it to call itself despite having no name.

Example:

```
"yes" printstr recurse
```

Will infinitely print `"yes"`.
