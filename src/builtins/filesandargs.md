# Files, Input, and CLI Arguments

This section provides a reference for various I/O related built-in functions.

- [afopen](afopen.md)
- [argc](argc.md)
- [eof](eof.md)
- [fgetch](fgetch.md)
- [flushinp](flushinp.md)
- [getarg](getarg.md)
- [getch](getch.md)
- [getnum](getnum.md)
- [getstr](getstr.md)
- [head](head.md)
- [rfopen](rfopen.md)
- [tail](tail.md)
- [wfopen](wfopen.md)
