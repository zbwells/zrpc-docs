# append

* arity: 2
* incptr: 1

Concatenates two strings together, although the first one must be a dynamically allocated buffer, either via [`emptybuf`](builtins/emptybuf.md), or via [`getstr`](builtins/getstr.md).

Example:

```
emptybuf "garbage" append
```

