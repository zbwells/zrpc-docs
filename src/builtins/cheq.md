# cheq

* arity: 1
* incptr: 1

Checks two characters and outputs 0 if they are different, and 1 if they are the same. Useful in conditional statements.

Accepts two arguments, despite what the arity would suggest. The first argument remains on the stack, and so the arity is still 1. Behavior subject to change depending on feedback.

Usage:

```
'a 'b' cheq =
```

Should print `0`, because the two characters are different.
