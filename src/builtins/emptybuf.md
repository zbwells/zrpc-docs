# emptybuf

* arity: 0
* incptr: 1

Creates an empty dynamically allocated string buffer on the stack, of size 2048.
More buffers need to be allocated if more than 2048 bytes are required for the target.

Example:

```
emptybuf "this" append "is" append "sparta" append
```

