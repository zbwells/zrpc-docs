# afopen

* arity: 1
* incptr: 1

Pops string off the stack, and passes string as location for filename to open. Opens file in append mode (file pointer starts at end of file, writing enabled).

Example:

```
"Add number to 'file.txt': " printstr pop 
"file.txt" afopen getstr fprintstr
```

Will append a number typed into stdin into "file.txt".
