# ascii

* arity: 1
* incptr: 1

Pops number off the stack, and if possible, converts it to its ascii value. Undefined behavior for floating point numbers.

Usage:

```
65 ascii
```

is equivalent to `A`. 


