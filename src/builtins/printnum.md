# =

* arity: 1
* incptr: 1

Prints out a number, and pops the number off the stack.

Usage:

```
<number> =
```

Example:

```
5 =
```




