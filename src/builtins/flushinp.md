# flushinp

* arity: 0
* incptr: 1

Removes any lingering characters from stdin before next usage of stdin is needed.
Legacy function— currently useless, as all functions that use stdin clear stdin before returning.


