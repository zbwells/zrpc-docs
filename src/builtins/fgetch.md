# fgetch

* arity: 0
* incptr: 1

Fetches a character from a file handler object.

Usage:

```
"file.txt" rfopen fgetch printchr
```

will print the first character from "file.txt".
