# unpop

* arity: n/a
* incptr: n/a

Increments the stack pointer by 1, without modifying the value of the object on the stack, effectively bringing back any data that was previously popped off, as long as nothing else had been pushed back in its place at any point. Very situational.

Example:

```
5 4 * unpop *
```

will result in `80`.
