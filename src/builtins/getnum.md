# getnum

* arity: 0
* incptr: 1

Gets a number from stdin, specifically puts it in numeric form. Undefined behavior if a string is entered.

Usage:

```
getnum 5 + =
```
