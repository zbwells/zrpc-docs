# c

* arity: n/a
* incptr: n/a

Sets the stack pointer back to 0, effectively clearing the stack.

Usage:

```
<program> c
```
