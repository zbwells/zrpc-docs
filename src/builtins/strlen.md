# strlen

* arity: 0
* incptr: 1

Gets the length of a string.

Usage:

```
"garbage" strlen =
```

will print `7`.
