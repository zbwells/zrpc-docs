# rfopen

* arity: 1
* incptr: 1

Opens a file in read mode.

Usage:

```
"file.txt" rfopen fgetch printchr
```

will print the first character from "file.txt".
