# Miscellaneous Stack-related Functions

This section provides a reference for varios stack-manipulation related built-in functions.

- [=](printnum.md)
- [c](clear.md)
- [d](duplicate.md)
- [pop](pop.md)
- [q](quit.md)
- [recurse](recurse.md)
- [swap](swap.md)
- [unpop](unpop.md)
