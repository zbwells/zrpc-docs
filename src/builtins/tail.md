# tail

* arity: 0
* incptr: 1

Copies a string, but removes the first character.

Usage:

```
"garbage" tail printstr
```

will print `arbage`.
