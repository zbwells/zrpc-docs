# eof

* arity: 0
* incptr: 1

End-of-file object. Equivalent to -1, or EOF in C.

Example:

```
(getstdin)
  getch d eof cheq 1 ?{
    q
  }
  
  printchr getstdin
:
```

This function echoes whatever data is input to stdin until an EOF is found, and then it terminates.
