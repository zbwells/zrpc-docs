# Functions

Function definitions in zrpc are done similarly to how they're done in Lisp-languages, but instead of something like `(define (fact n) <function body>)`, you would instead just do `(fact n) <function body> :`. 

Functions in zrpc can have any number of arguments, and to put variables on the stack from the functions, write the name of the parameter inside the function body, and the item with that name will be placed on the stack.

Parameters are all Pass-by-value, excluding strings, where a pointer to the string is passed instead— the language does not yet support passing dynamically allocated strings back and forth between functions, although it is very much possible— it just creates untrapped memory leaks.

See the [Syntax](syntax.md) page for a full example of a simple function. 
