# Logic

Logic works via logical built-in functions, each of which accept two parameters— specifically 

* `x y ?{` 
* `x y !?{` 
* `x y <?{` 
* `x y >?{`

which correspond to 

* `if (x == y)` 
* `if (x != y)` 
* `if (x < y)`
* `if (x > y)` 

respectively.

Each of these logical functions only accept numeric data as parameters— passing strings or characters will result in undefined behavior, although in some cases it may appear to work properly.

In order to use these with strings or characters, use `streq` and `cheq`. These functions check equality between strings or between characters. See [Strings and Text Processing](stringbuiltins.md) for more information on `streq` and `cheq`.

Here is a sample program containing conditionals:

```
; Program to print the fibbonacci sequence to the nth term. 

#main   "How many fibbonacci numbers should be printed? \n=> " 
        printstr getnum prnfibs :

(fib n)
    n 3 <?{
        1 <-
    }
  
    n 1 - fib n 2 - fib + <-
:

(prnfibs n)
    n 0 >?{
        n fib n 1 - prnfibs pop = nl
    }
    
    <-
:

@main
```

The above program can be found at the git repo: [`progs/nfibs.rpc`](https://codeberg.org/zbwells/zrpc/src/branch/master/progs/nfibs.rpc).


