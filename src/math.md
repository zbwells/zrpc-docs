# Math

Math in zrpc is all postfix, and somewhat resembles the Unix arbitrary precision calculator `dc`.

Here is an example:

```
5 4 + 10 * 2 -
```

5 and 4 will be put on the stack, and then added—

```
9 10 * 2 -
```

and then 10 will be put on the stack, and 9 and 10 will be multiplied—

```
90 2 -
```

and finally, 90 and 2 will be popped off the stack and subtracted, and the result will be:

```
88
```

This is the gist of how all math executes in zrpc. The kicker is that all other functions also operate in this way, as all functions accept function arguments in post-fix syntax, including user-defined ones.
