# Macros

Macros in zrpc are equivalent to C `#define` preprocessor directives.

Here is an example of how one might be used:

```
#limit 128

@limit 127 >?{
    "127 is within the limit" q
}
```

Wherever `@` and the name of the macro is, that text will be replaced with the text defined by the macro. This is idiomatically used to place toplevel statements above functions, which is normally not possible. Most of the sample programs provided in `/progs` on the git repo use this form.

```
#main emptybuf listhandle :

(listhandle list)
  list strlen 1 >?{
    "List: " printstr pop
    list printstr nl
  }
  
  "Enter a number to add to the list (ctrl+c to quit): " printstr pop
  list getstr append " " append
  
  listhandle
:

@main
```
    
