# Strings

Strings in zrpc are handled different depending on whether they are string-literals: something like `"hello"` or `"goodbye"`, text surrounded by double-quotes written directly in the source code, or whether they are dynamically allocated char-arrays, as in the case of user-input strings such as generated through `getstr` or `emptybuf`.

There are many built-ins that can be used to manipulate strings, although some of them (`append` in particular) can only be used with dynamically allocated strings.

Here is a sample program (available at [`progs/list.rpc`](https://codeberg.org/zbwells/zrpc/src/branch/master/progs/list.rpc)):

```
;; program to test string appending functionality

#main emptybuf listhandle :

(listhandle list)
  list strlen 1 >?{
    "List: " printstr pop
    list printstr nl
  }
  
  "Enter a number to add to the list (ctrl+c to quit): " printstr pop
  list getstr append " " append
  
  listhandle
:

@main
```


