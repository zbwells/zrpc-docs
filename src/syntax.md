# Syntax

The syntax for zrpc is all reverse-polish, except for places where it's not. The preprocessor and the user-defined function parsing components are not reverse polish.

To get a better look at the syntax, take a look at a sample program, piece by piece— in this case, [`progs/hellotimes.rpc`](https://codeberg.org/zbwells/zrpc/src/branch/master/progs/hellotimes.rpc) from the git repo.

This program asks for a number, and then prints out "Hello World!" the amount of times specified by the number. 

```
;; Hello World! however many times 

(hellotimes num)
	num 0 ?{
		q
	}
	
	"Hello World! " printstr pop
	num 1 - hellotimes <-
:

5 hellotimes nl
```

The program starts out with a comment, which is delimited by a pair of semicolons:

```
;; Hello World! however many times 
```

And then immediately defines a function:

```
(hellotimes num)
```

This function is named "hellotimes", and takes one argument, called `num`.

```
num 0 ?{
	q
}
```

The program then uses a conditional (equivalent to "if num == 0 { exit() }") to check to see if the number is equal to zero, and if it is, it exits the program.

```
"Hello World! " printstr pop
```

Otherwise, it prints `Hello World! `, and then pops the string off the stack manually to get it out of the way—

```
num 1 - hellotimes <-
```

and then to complete the recursive loop, subtracts 1 from num, and then passes it to hellotimes— this will continue until the terminal condition is met (num being 0), and the function terminates. The `<-` signifies a function return.

```
:
```

All functions and macros must be terminated with a colon.

Any operations outside of a function or macro must come after any defined functions or macros. 

```
5 hellotimes nl
```

The program starts off by calling `hellotimes` with a 5, meaning that `Hello World!` should be printed 5 times, and after `hellotimes` finishes executing, the program ends by printing a newline.

A more comprehensive understanding of the syntax can be gained by reading more sample programs. A better guide might also be written at some point.

	
