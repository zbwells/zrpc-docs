# Recursion

Recursion operates in zrpc much in the way it does in other programming languages.  Each user-defined function in zrpc generates an equivalent function in C, and so the properties of recursion more or less mimic the properties of recursion in C. (i.e. there is no tail-call optimization.)

The typical sample program for recursion tends to be a factorial function, which is available at [`progs/fact.rpc`](https://codeberg.org/zbwells/zrpc/src/branch/master/progs/fact.rpc).

Here is the text:

```
;; Calculates factorials of numbers

#main     
  getarg "\0" streq 1 ?{ 
    q 
  } 
  
  atof fact = nl 
:

(fact n)  
  n 1 ?{ 
    n <- 
  } 
  
  n 1 - fact * <- 
:

@main
```

This particular version grabs a number from the command-line arguments, and computes the factorial of that number, rather than asking for a number once the program starts.
